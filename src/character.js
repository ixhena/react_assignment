import { useEffect, useState } from 'react';
import { Card, Container, Row, Col, ListGroup, ListGroupItem, Navbar } from 'react-bootstrap';

function Character() {
    const apiUrl = 'https://rickandmortyapi.com/api/character';
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [data, setData] = useState([]);
    useEffect(() => {
        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setData(result.results);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <>
                <Navbar bg="dark">
                    <Container>
                        <Navbar.Brand href="#home">
                            <img
                                src="https://www.pngitem.com/pimgs/m/43-438051_rick-and-morty-title-hd-png-download.png"
                                className="d-inline-block align-top"
                                alt="React Bootstrap logo"
                            />
                        </Navbar.Brand>
                    </Container>
                </Navbar>
                <Container style={{ columns: 2, marginTop: "97px" }}>
                    <Row>
                        <Col>
                            {data.map(character => (
                                <Card key={character.id} >
                                    <Card.Img variant="top" src={character.image} />
                                    <Card.Body>
                                        <ListGroup className="list-group-flush" >
                                            <Card.Title className='name'>{character.name}</Card.Title>
                                            <p className='informationStyle'>Gender:</p> <ListGroupItem>{character.gender}</ListGroupItem>
                                            <p className='informationStyle'>Created:</p> <ListGroupItem>{character.created}</ListGroupItem>
                                            <p className='informationStyle'>Species:</p><ListGroupItem>{character.species}</ListGroupItem>
                                            <p className='informationStyle'>Status:</p><ListGroupItem>{character.status}</ListGroupItem>
                                            <p className='informationStyle'>Location:</p><ListGroupItem>{character.location.name}</ListGroupItem>
                                            <p className='informationStyle'>Origin:</p><ListGroupItem>{character.origin.name}</ListGroupItem>
                                        </ListGroup>
                                    </Card.Body>
                                </Card>
                            ))}</Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default Character;
